# SQLController

The SQLController class project aims to create a neat binding between your PHP code and SQL database. It's really small, just around 10kB and easy to work with. The SQLController is highly flexible and feature-rich despite the size.

## Acknowledgement

This class is by no means perfect, it's a work in progress and can probably be improved in many ways and areas. I have been using it for quite some time and it seems to be working very nicely. If you are interested in contributing don't hesitate to contact me.

## Features

* SQL connection using [PHP Data Objects (PDO)][1]
* Simple setup; just extend your class with SQLController and get working.
* Safe queries; SQLController uses prepared statements in all queries.
* Table relations; setting up and working with table-relations is simple.
* Object oriented SQL; you work with instances of your classes instead of sets of data.

## Basic Usage

Using SQLController is really simple, you just include it in your project and implement it on a class you want to communicate with a SQL table. By default the SQLController expect an auto-increment "id" column to reference the correct row.

Include and connect to sql server.
	
	<?php
		require_once("SQLController.php");
		SQLController::connect("localhost", "username", "pass", "database");
	?>

Create a class that handles a single table
	
	<?php
		class UsersTable extends SQLController {
			public function __construct() {
				$this->tableName = "users"; // SQL table name
			}
		}
	
		// Instantiate
		$usersTable = new UsersTable();
	
		// Fetch all rows
		$myUsers = $usersTable->getAll(); // array
	
		// Fetch row by ID
		$byID = $usersTable->getByID(1); // distinct
	
		// Fetch row by specific column
		$byField = $usersTable->getByField("name", "Stig Rune"); // array
	?>

## Inserting Rows

To insert a row you create a new instance of the correct class and set properties matching table fields.

	<?php
		// Save a new user
		$user = new UsersTable();
		$user->name = "Stig Rune";
		$user->password = "passw0rd";
		$user->country = "Norway";
		$user = $user->save(); // save() returns new object with "id" set if saved correctly
	?>

Calling `save()` here will result in a mysql INSERT query.

## Updating Rows

If you fetch a row using one of the examples explained earlier a "id" property will be set on the object. Updating properties and calling save on such an object will result in a mysql UPDATE query.

	<?php
		// Update an old user
		$usersTable = new UsersTable();
		$user = $usersTable->getByID(1);
	
		// Update "country" then save
		$user->country = "Denmark";
		$user->save();
	?>

## Deleting Rows

Deleting rows is done by calling `delete()` on an object. Note that if a relation is setup (more on that later) all children will also be deleted to not have "unlinked" rows in the database.

	<?php
		// Delete a user
		$usersTable = new UsersTable();
		$user = $usersTable->getByID(1);
		$user->delete();
	?>

## Relational Tables

To setup an relation between to or more tables you use the `$relations` and `$relation` properties.

Say you have two tables, one for users and one for comments which are related to the user who wrote it. Both tables have an auto-increment "id" field, the "comments" table also have an "user_id" field which corresponds to the "id" field of table "users".

	<?php
		class User extends SQLController {
			public function __construct() {
				$this->tableName = "users";
				$this->relations =  [
					new OneToMany("Comments", "id")
				];
			}
		}
	
		class Comments extends SQLController {
			public function __construct() {
				$this->tableName = "comments";
				$this->relation = new ManyToOne("User", "user_id");
			}
		}
	?>

Now you can reference rows from the "comments" table using the User class.

	<?php
		$user = new User();
		$user->getByID(10);
		
		// Get all comments with user_id set to 10
		$comments = $user->Comments()->getAll();
	
		// Create a new comment using the $user object as factory to make it related
		$new_comment = new $user->Comments();
		$new_comment->text = "Sweet!";
		$new_comment->save();
	
		// You can also nest these calls
		$new_comment->text("Sweet!")->save();
	?>

**When deleting a user in this case all the comments linked to that user is deleted as well.**

## Relation Type

When you create a new relation you use `OneToMany` or `ManyToOne` but there is no practical difference between the two. They are both extending the `SQLRelation` class and are only meant for readability.

From the SQLController.php source code:

	<?php
		class SQLRelation {
			function __construct($class, $connector, $foreign = "") {
				$this->class = $class;				// The relational class.
				$this->connector = $connector;		// The relational field.
	
				if ($foreign) {
					$this->foreignKey = $foreign;	// The foreign relational field.
				} else {
					$this->foreignKey = $connector;
				}
			}
		}
		class OneToMany extends SQLRelation {}
		class ManyToOne extends SQLRelation {}
	?>

## Advanced Queries

To do more advanced select queries you call the `selectAll()` factory method and build upon in. The `selectAll()` method returns an `SQLQuery` object with the correct relations setup. The `SQLQuery` object currently works with sql "WHERE", "ORDER BY", "LIMIT" and "OFFSET".

Using the previous example with users and comments we can, for example, do the following:

	<?php
		$comments = $user->Comments()->selectAll()->orderBy("datetime", SQLQuery::DESCENDING)->limit(10)->execute();
	?>

Or we can do a search:

	<?php
		$comments = $user->Comments()->selectAll()->where("text", "%sweet%", SQLQuery::EQUALS)->execute();
	?>

You can also specify an array of fields and values you want to use with sql WHERE:

	<?php
		$conditions = [
			["name", "%sweet%", SQLQuery::SQL_LIKE],
			["reads" 100, SQLQuery::MORE_THAN_EQUALS]
		];
	
		$comments = $user->Comments()->selectAll()->where($conditions)->execute();
	?>

## Custom Queries

If you need to do custom queries you can call `parent::sql()` which references the PDO database handler. I normally use this via a function on my class in some special cases.

	<?php
		class Users extends SQLController {
			public function __construct() {
				$this->tableName = "users";
			}
	
			public static function clearInactiveUsers() {
				$query = "DELETE FROM {$this->tableName} WHERE last_online < DATE_SUB(NOW(), INTERVAL 1 MONTH)";
				parent::sql()->query($query);
			}
		}
	
		// This is harsh
		Users::clearInactiveUsers();
	?>

[1]: http://php.net/manual/en/book.pdo.php