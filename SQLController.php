<?php
	/*
	 *	SQLController represents 1 single table in a database.
	 */
	abstract class SQLController {
		/*
		 *	Connection identifier.
		 */
		private static $dbh;
		public static function connect($host, $username, $password, $database) {
			try {
				self::$dbh = new PDO("mysql:host={$host};dbname={$database};charset=utf8;", $username, $password, [PDO::ATTR_PERSISTENT => true]);
				self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			}
			catch (PDOException $e) {
				return $e->GetMessage();
			}

			return true;
		}
		public static function sql() {
			if (! self::$dbh) return false;
			return self::$dbh;
		}

		/*
		 *	Private properties
		 */
		private $tableName = "";		// The name of the table
		private $fields = [];			// Array of fields (columns) in table
		private $relations = [];		// Array of downwards relations as 1 User -> Many comments
		private $relation = "";			// Upwards relation as Many comments -> 1 user

		/*
		 *	Create the object with tablename if not already set.
		 */
		public function __construct() {
			if (! $this->tableName) {
				$this->tableName = strtolower(get_class($this)) . "s";
			}

			return $this;
		}

		/*
		 *	Used to dynamically handle reading of fields.
		 */
		public function __get($var) {
			if (isset($this->$var)) {
				return $this->$var;
			}
			else {
				if (array_key_exists($var, $this->fields)) {
					if ($this->fields[$var] == "null") {
						return "";
					}
					else {
						return $this->fields[$var];
					}
				}
			}

			// Variable didn't exist.
			$class = get_class($this);
			die("Variable: '{$var}' does not exist under instance of class: '{$class}'");
		}

		/*
		 *	Used to dynamically handle writing of field values.
		 */
		public function __set($var, $value) {
			if (isset($this->$var)) {
				$this->$var = $value;
				return $this;
			}
			else {
				$this->fields[$var] = $value;
				return $this;
			}

			// Variable didn't exist.
			$class = get_class($this);
			die("Variable: '{$var}' does not exist under instance of class: '{$class}'");
		}

		/*
		 *	Used to dynamically handle realationships between tables.
		 */
		public function __call($function, $vars) {
			// We are trying to work with an "relation"
			foreach ($this->relations as $relation) {
				if ($function == $relation->class) {
					$new_class = new $relation->class;
					$new_class->{$relation->foreignKey} = $this->{$relation->connector};

					return $new_class;
				}
			}

			// Fatal error, most likely because the relationships aren't setup properly.
			$class = get_class($this);
			die("Called to undefined method in instance of class {$class}. Check your spelling and relationship setup.");
		}

		/*
		 *
		 */
		public function toJSON() {
			return json_encode($this->fields);
		}

		/*
		 *	Factory for creating a select query.
		 */
		public function selectAll() {
			if ($this->relation) {
				$query = new SQLQuery(get_class($this), $this->tableName, $this->relation->connector, $this->{$this->relation->connector});
			} else {
				$query = new SQLQuery(get_class($this), $this->tableName);
			}
			return $query;
		}

		/*
		 *	Get 1 specific row by id.
		 */
		public function getByID($id) {
			if ($this->relation) {
				$query = "SELECT DISTINCT * FROM {$this->tableName} WHERE id = ? AND {$this->relation->connector} = '{$this->{$this->relation->connector}}'";
			} else {
				$query = "SELECT DISTINCT * FROM {$this->tableName} WHERE id = ?";
			}

			$statement = SQLController::sql()->prepare($query);
			$statement->execute([$id]);
			$object = $statement->fetch(PDO::FETCH_ASSOC);

			if ($object) {
				return $this->createInstance($object);
			}

			return false;
		}

		/*
		 *	Get all rows with a specific value in field.
		 */
		public function getByField($field, $value) {
			if ($this->relation) {
				$query = "SELECT * FROM {$this->tableName} WHERE {$field} = ? AND {$this->relation->connector} = '{$this->{$this->relation->connector}}'";
			} else {
				$query = "SELECT * FROM {$this->tableName} WHERE {$field} = ?";
			}

			$statement = SQLController::sql()->prepare($query);
			$statement->execute([$value]);
			$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
			$objects = [];

			foreach ($rows as $row) {
				$objects[] = $this->createInstance($row);
			}

			return $objects;
		}

		/*
		 *	Get all items based on relationship.
		 */
		public function getAll() {
			if ($this->relation) {
				$query = "SELECT * FROM {$this->tableName} WHERE {$this->relation->connector} = '{$this->{$this->relation->connector}}'";
			}
			else {
				$query = "SELECT * FROM {$this->tableName}";
			}

			$rows = SQLController::sql()->query($query)->fetchAll(PDO::FETCH_ASSOC);
			$objects = [];

			foreach ($rows as $row) {
				$objects[] = $this->createInstance($row);
			}

			return $objects;
		}

		/*
		 *	Count all items in table.
		 */
		public function count() {
			if ($this->relation) {
				$query = "SELECT id FROM {$this->tableName} WHERE {$this->relation->connector} = '{$this->{$this->relation->connector}}'";
			}
			else {
				$query = "SELECT id FROM {$this->tableName}";
			}

			$rows = SQLController::sql()->query($query)->fetchAll();
			return count($rows);
		}

		/*
		 *	INSERT or UPDATE item based on previous state.
		 */
		public function save() {
			if (isset($this->fields['id'])) {
				// Update the row

				// Create a string of key value pairs for update statement.
				$values = [];
				$key_values = "";
				foreach ($this->fields as $key => $value) {
					if ($key != "id") {
						$key_values .= "{$key} = ?, ";
						$values[] = $value;
					}
				}
				$key_values = rtrim($key_values, ", ");

				// Prepare statement and execute
				$query = "UPDATE {$this->tableName} SET {$key_values} WHERE id = '{$this->fields['id']}'";
				$statement = SQLController::sql()->prepare($query);
				$statement->execute($values);

			} else {
				// Insert the row

				// Get values and keys
				$values = array_values($this->fields);
				$keys = implode(",", array_keys($this->fields));

				// Create query with correct keys
				$valueStr = str_repeat("?,", count($values));
				$valueStr = rtrim($valueStr, ",");
				$query = "INSERT INTO {$this->tableName} ({$keys}) VALUES ({$valueStr})";

				// Prepare statement and execute
				$statement = SQLController::sql()->prepare($query);
				$statement->execute($values);

				// Get and set last id
				$this->id = SQLController::sql()->lastInsertId();
			}

			// Return self, with new id if inserted
			return $this;
		}

		/*
		 *	DELETE item and children.
		 */
		public function delete() {
			if ($this->id) {
				$query = "DELETE FROM {$this->tableName} WHERE id = ?";

				$statement = SQLController::sql()->prepare($query);
				$statement->execute([$this->id]);

				// Delete the children.
				$this->deleteChildren();
			}
		}

		/*
		 *	DELETE all children of item.
		 */
		public function deleteChildren() {
			foreach ($this->relations as $relation) {
				$instance = new $relation->class;
				$instance->{$relation->foreignKey} = $this->{$relation->connector};

				// Get all the children entries in database.
				$entries = $instance->getAll();

				foreach ($entries as $entry) {
					$entry->delete();
				}
			}
		}

		/*
		 *	createInstance - used in all methods that returns instance/array of instances.
		 *	Creates a new instance and populates fields with data from query.
		 */
		public function createInstance($args) {
			$new_class = new $this;

			foreach ($args as $key => $value) {
				$new_class->fields[$key] = $args[$key];
			}

			return $new_class;
		}
	}

	/*
	 *	Class used by SQLController to tell how it relates to other tables.
	 */
	class SQLRelation {
		function __construct($class, $connector, $foreign = "") {
			$this->class = $class;				// The relational class.
			$this->connector = $connector;		// The relational field.

			if ($foreign) {
				$this->foreignKey = $foreign;	// The foreign relational field.
			} else {
				$this->foreignKey = $connector;
			}
		}
	}

	// For readability.
	class OneToMany extends SQLRelation {}
	class ManyToOne extends SQLRelation {}

	/*
	 *	Query factory, used from SQLController->query.
	 */
	class SQLQuery {
		const ASCENDING = "ASC";
		const DESCENDING = "DESC";
		const SQL_LIKE = "LIKE";
		const EQUALS = "=";
		const NOT_EQUAL = "!=";
		const LESS_THAN = "<";
		const MORE_THAN = ">";
		const LESS_THAN_EQUALS = "<=";
		const MORE_THAN_EQUALS = ">=";

		public function __construct($controller, $tablename, $foreign_key = "", $foreign_value = "") {
			$this->query = "";
			$this->controller = $controller;
			$this->tableName = $tablename;
			$this->foreignKey = $foreign_key;
			$this->foreignValue = $foreign_value;
			$this->prepareValues = [];
			$this->whereStatementFlag = False;
		}

		public function where($key, $value = "", $operator = SQLQuery::EQUALS) {
			$this->query = "SELECT * FROM {$this->tableName} WHERE ";

			if ($this->foreignKey and $this->foreignValue) {
				$this->query .= "{$this->foreignKey} = ? AND ";
				$this->prepareValues[] = $this->foreignValue;
			}

			if (is_array($key)) {
				foreach ($key as $k) {
					$operator = isset($k[2]) ? $k[2] : SQLQuery::EQUALS;
					$this->query .= "{$k[0]} {$operator} ? AND ";
					$this->prepareValues[] = $k[1];
				}
				$this->query = substr($this->query, 0, count($this->query)-6);
			} else {
				$this->query .= "$key $operator ?";
				$this->prepareValues[] = $value;
			}

			$this->whereStatementFlag = True;

			return $this;
		}

		public function orderBy($column, $orientation = SQLQuery::ASCENDING) {
			if (is_array($column)) {
				$this->query .= " ORDER BY ";

				foreach ($column as $order) {
					$this->query .= "{$order[0]} {$order[1]}, ";
				}
				$this->query = substr($this->query, 0, count($this->query)-3);
			} else {
				$this->query .= " ORDER BY {$column} {$orientation}";
			}

			return $this;
		}

		public function limit($rows) {
			$this->query .= " LIMIT {$rows}";

			return $this;
		}

		public function offset($offset) {
			$this->query .= " OFFSET {$offset}";

			return $this;
		}

		public function execute() {
			if ($this->whereStatementFlag == False) {
				if ($this->foreignKey and $this->foreignValue) {
					$this->query = "SELECT * FROM {$this->tableName} WHERE {$this->foreignKey} = ? " . $this->query;
					$this->prepareValues[] = $this->foreignValue;
				} else {
					$this->query = "SELECT * FROM {$this->tableName} " . $this->query;
				}
			}

			$statement = SQLController::sql()->prepare($this->query);
			$statement->execute($this->prepareValues);

			$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
			$objects = [];

			if ($rows) {
				$dummy = new $this->controller;

				foreach ($rows as $row) {
					$objects[] = $dummy->createInstance($row);
				}
			}

			return $objects;
		}
	}
?>